%% -*- erlang-indent-level: 4;indent-tabs-mode: nil -*-
%% ex: ts=4 sw=4 et
%%
%% Copyright 2013-2018 Triq authors
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%
%% @author Richard Carlsson <carlsson.richard@gnail.com>
%% @author Kresten Krab Thorup <krab@trifork.com>
%% @copyright 2006-2014 Richard Carlsson
%% @private
%% @see triq
%% @doc Parse transform for automatic exporting of prop_ functions.

-module(triq_autoexport).

-define(DEFAULT_PROP_PREFIX, "prop_").

-export([parse_transform/2]).

-define(CHECK,check).

parse_transform(Forms, Options) ->
    %% io:format("FORMS ~n~p~n", [Forms]),
    PropPrefix = proplists:get_value(triq_prop_prefix, Options,
                                     ?DEFAULT_PROP_PREFIX),
    F = fun (Form, Set) ->
                t_form(Form, Set, PropPrefix)
        end,
    PropExports = sets:to_list(lists:foldl(F, sets:new(), Forms)),
    EUnit = maybe_gen_eunit(PropPrefix, Forms),
    EUnitExports = lists:map(fun ({function, _, Name, 0, _}) -> {Name, 0} end, EUnit),
    Forms1 = t_rewrite(Forms, PropExports ++ EUnitExports),
    add_eunit(Forms1, EUnit).

t_form({function, _L, Name, 0, _Cs}, S, PropPrefix) ->
    N = atom_to_list(Name),
    case lists:prefix(PropPrefix, N) of
        true ->
            sets:add_element({Name, 0}, S);
        false ->
            S
    end;
t_form(_, S, _) ->
    S.

gen_eunit(Forms, PropPrefix, Opts) ->
    EUnitGen = fun(Form, Set) ->
                       t_eunit_form(Form, Set, PropPrefix, Opts)
               end,
    sets:to_list(lists:foldl(EUnitGen, sets:new(), Forms)).

lists_last([]) -> [];
lists_last(L) -> lists:last(L).

maybe_gen_eunit(PropPrefix, Forms) ->
    TriqAttrs = lists:foldl(
                  fun({attribute,_,triq,Val}, Acc) ->
                          [Val|Acc];
                     (_, Acc) ->
                          Acc
                  end,
                  [], Forms),
    Attrs = lists_last(proplists:lookup_all(eunit, TriqAttrs)),
    case Attrs of
        {eunit, true} ->
            gen_eunit(Forms, PropPrefix, []);
        {eunit, Opts} ->
            gen_eunit(Forms, PropPrefix, Opts);
        _ ->
            []
    end.

check_args(Name, Anno, Opts) ->
    case lists_last(proplists:lookup_all(runs, Opts)) of
        [] ->
            F = [{call,Anno,{atom,Anno,list_to_atom(Name)},[]}],
            {F, "triq : check ( "++Name++" ( ) )"};
        {runs, Runs} ->
            F = [{call,Anno,{atom,Anno,list_to_atom(Name)},[]},
                 {integer,Anno,Runs}],
            {F, "triq : check ( "++Name++" ( ), "++integer_to_list(Runs)++" )"}
    end.

assertion(Name, Anno, Opts) ->
    "prop_" ++ PropName = Name,
    TestName = list_to_atom(PropName ++ "_test_"),
    {CheckArgs, CheckCallStr} = check_args(Name, Anno, Opts),

    {function,Anno,TestName,0,
     [{clause,Anno,[],[],
       [{tuple,Anno,
         [{atom,Anno,timeout},
          {integer,Anno,3600},
          {tuple,Anno,
           [{integer,Anno,erl_anno:line(Anno)},
            {'fun',Anno,
             {clauses,
              [{clause,Anno,[],[],
                [{block,Anno,
                  [{call,Anno,
                    {'fun',Anno,
                     {clauses,
                      [{clause,Anno,[],[],
                        [{'case',Anno,
                          {call,Anno,
                           {remote,Anno,{atom,Anno,triq},{atom,Anno,check}},
                           CheckArgs},
                          [{clause,Anno,[{atom,Anno,true}],[],[{atom,Anno,ok}]},
                           {clause,Anno,
                            [{var,Anno,'__V'}],
                            [],
                            [{call,Anno,
                              {remote,Anno,{atom,Anno,erlang},{atom,Anno,error}},
                              [{tuple,Anno,
                                [{atom,Anno,assertion_failed},
                                 {cons,Anno,
                                  {tuple,Anno,
                                   [{atom,Anno,module},{atom,Anno,mutations}]},
                                  {cons,Anno,
                                   {tuple,Anno,
                                    [{atom,Anno,Anno},{integer,Anno,Anno}]},
                                   {cons,Anno,
                                    {tuple,Anno,
                                     [{atom,Anno,expression},
                                      {string,Anno,CheckCallStr}]},
                                    {cons,Anno,
                                     {tuple,Anno,
                                      [{atom,Anno,expected},{atom,Anno,true}]},
                                     {cons,Anno,
                                      {tuple,Anno,
                                       [{atom,Anno,value},
                                        {'case',Anno,
                                         {var,Anno,'__V'},
                                         [{clause,Anno,
                                           [{atom,Anno,false}],
                                           [],
                                           [{var,Anno,'__V'}]},
                                          {clause,Anno,
                                           [{var,Anno,'_'}],
                                           [],
                                           [{tuple,Anno,
                                             [{atom,Anno,not_a_boolean},
                                              {var,Anno,'__V'}]}]}]}]},
                                      {nil,Anno}}}}}}]}]}]}]}]}]}},
                    []}]}]}]}}]}]}]}]}.

add_eunit([Form|[]], Eunit) ->
    [Form|Eunit];
add_eunit([Form|Rest], Eunit) ->
    [Form|add_eunit(Rest,Eunit)];
add_eunit([],_Eunit ) ->
    [].


t_eunit_form({function, Anno, Name, 0, _Cs}, S, PropPrefix, Opts) ->
    N = atom_to_list(Name) ,
    case lists:prefix(PropPrefix, N) of
        true ->
            Assertion = assertion(N, Anno, Opts),
            sets:add_element(Assertion, S);
        false ->
            S
    end;
t_eunit_form(_, S, _, _) ->
    S.

t_rewrite([{attribute,_,module,{Name,_Ps}}=M | Fs], Exports) ->
    module_decl(Name, M, Fs, Exports);
t_rewrite([{attribute,_,module,Name}=M | Fs], Exports) ->
    module_decl(Name, M, Fs, Exports);
t_rewrite([F | Fs], Exports) ->
    [F | t_rewrite(Fs, Exports)];
t_rewrite([], _Exports) ->
    [].            % fail-safe, in case there is no module declaration

rewrite([{function,_,?CHECK,0,_}=F | Fs], As, Module, _GenQC) ->
    rewrite(Fs, [F | As], Module, false);
rewrite([F | Fs], As, Module, GenQC) ->
    rewrite(Fs, [F | As], Module, GenQC);
rewrite([], As, Module, GenQC) ->
    {if GenQC ->
             [{function,0,?CHECK,0,
               [{clause,0,[],[],
                 [{call,0,{remote,0,{atom,0,triq},{atom,0,check}},
                   [{atom,0,Module}]}]}]}
              | As];
        true ->
             As
     end,
     GenQC}.

module_decl(Name, M, Fs, Exports) ->
    Module = Name,
    {Fs1, GenQC} = rewrite(Fs, [], Module, true),
    Es = if GenQC -> [{?CHECK,0} | Exports];
            true -> Exports
         end,
    [M, {attribute,0,export,Es} | lists:reverse(Fs1)].
